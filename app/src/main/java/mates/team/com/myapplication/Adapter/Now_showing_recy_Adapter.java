package mates.team.com.myapplication.Adapter;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import mates.team.com.myapplication.Modal.Now_showing_recy_Modal;
import mates.team.com.myapplication.R;

import java.util.ArrayList;

public class Now_showing_recy_Adapter extends RecyclerView.Adapter<Now_showing_recy_Adapter.Myholder> {

    ArrayList<Now_showing_recy_Modal> movie;
    Context context;

    public Now_showing_recy_Adapter(ArrayList<Now_showing_recy_Modal> movie, Context context) {
        this.movie = movie;
        this.context = context;
    }

    @NonNull
    @Override
    public Now_showing_recy_Adapter.Myholder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View itemView = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.now_showing_recy_adapter, viewGroup, false);
        return new Now_showing_recy_Adapter.Myholder(itemView);
    }

    @Override
    public void onBindViewHolder(@NonNull Now_showing_recy_Adapter.Myholder myholder, final int position) {
        myholder.name.setText(movie.get(position).getName());
        myholder.description.setText(movie.get(position).getDescription());
        myholder.language.setText(movie.get(position).getLanguage());
        myholder.experience.setText(movie.get(position).getExperience());
        myholder.id.setText(movie.get(position).getId());
        myholder.img.setImageResource(movie.get(position).getImage());
    }

    @Override
    public int getItemCount() {
        return movie.size();
    }

    public class Myholder extends RecyclerView.ViewHolder {
        TextView name, description, language, experience, id;
        ImageView img;
        public Myholder(@NonNull View itemView) {
            super(itemView);

            name = (TextView) itemView.findViewById(R.id.tv);
            description = (TextView) itemView.findViewById(R.id.tv1);
            language = (TextView) itemView.findViewById(R.id.tv2);
            experience = (TextView) itemView.findViewById(R.id.tv3);
            id = (TextView) itemView.findViewById(R.id.tv4);
            img = (ImageView) itemView.findViewById(R.id.img);


        }

    }
}
