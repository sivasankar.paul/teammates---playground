package mates.team.com.myapplication.Adapter;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import mates.team.com.myapplication.Modal.Coming_Soon_recy_Modal;
import mates.team.com.myapplication.R;

import java.util.ArrayList;

public class Coming_soon_recy_Adapter extends RecyclerView.Adapter<Coming_soon_recy_Adapter.Myholder> {

    ArrayList<Coming_Soon_recy_Modal> movies;
    Context context;

    public Coming_soon_recy_Adapter(ArrayList<Coming_Soon_recy_Modal> movie, Context context) {
        this.movies = movie;
        this.context = context;
    }

    @NonNull
    @Override
    public Myholder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View itemView = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.coming_soon_recy_adapter, viewGroup, false);
        return new Coming_soon_recy_Adapter.Myholder(itemView);
    }

    @Override
    public void onBindViewHolder(@NonNull final Coming_soon_recy_Adapter.Myholder myholder, final int position) {
        myholder.name.setText(movies.get(position).getName());
        myholder.description.setText(movies.get(position).getDescription());
        myholder.language.setText(movies.get(position).getLanguage());
        myholder.experience.setText(movies.get(position).getExperience());
        myholder.id.setText(movies.get(position).getId());
        myholder.img1.setImageResource(movies.get(position).getImage1());
        myholder.img2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Log.e("fav", "bookmark " + movies.get(position).getaBoolean());
                if (movies.get(position).getaBoolean()) {
                    movies.get(position).setaBoolean(false);
                } else {
                    movies.get(position).setaBoolean(true);
                }
                notifyDataSetChanged();
            }
        });


        if (movies.get(position).getaBoolean()) {
            myholder.img2.setImageResource(R.drawable.ic_favorite_heart_button_yellow);
        } else {
            myholder.img2.setImageResource(R.drawable.ic_favorite_heart_button);
        }

    }

    @Override
    public int getItemCount() {
        return movies.size();
    }

    public class Myholder extends RecyclerView.ViewHolder {
        TextView name, description, language, experience, id;
        ImageView img1, img2;

        public Myholder(@NonNull View itemView) {
            super(itemView);
            name = (TextView) itemView.findViewById(R.id.tv);
            description = (TextView) itemView.findViewById(R.id.tv1);
            language = (TextView) itemView.findViewById(R.id.tv2);
            experience = (TextView) itemView.findViewById(R.id.tv3);
            id = (TextView) itemView.findViewById(R.id.tv4);
            img1 = (ImageView) itemView.findViewById(R.id.img);
            img2 = (ImageView) itemView.findViewById(R.id.img_fav_heart);

        }
    }
}
