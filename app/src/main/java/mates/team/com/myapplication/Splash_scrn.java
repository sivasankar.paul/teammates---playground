package mates.team.com.myapplication;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.os.Handler;
import android.support.v7.app.AppCompatActivity;
import android.widget.ImageView;
import android.widget.ProgressBar;

public class Splash_scrn extends AppCompatActivity {
ImageView img;
    ProgressBar progressBar;
    SharedPreferences sharedPreferences;
    public static final String MYPREFERENCE = "MyPrefs";


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash_scrn);

        sharedPreferences = getSharedPreferences(MYPREFERENCE, Context.MODE_PRIVATE);
        progressBar = findViewById(R.id.progress_bar);

        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                boolean is_user_login = sharedPreferences.getBoolean("is_login", false);
                if (is_user_login) {
                    // user details saved
                    Intent mainIntent = new Intent(Splash_scrn.this, Home_page.class);
                    startActivity(mainIntent);
                } else {
                Intent intent = new Intent(Splash_scrn.this,MainActivity.class);
                startActivity(intent);
            }}
        },2000);
    }
}
