package mates.team.com.myapplication.Fragment;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import mates.team.com.myapplication.Adapter.Coming_soon_recy_Adapter;
import mates.team.com.myapplication.Modal.Coming_Soon_recy_Modal;
import mates.team.com.myapplication.R;

import java.util.ArrayList;

public class HP_Coming_Soon_Frag extends Fragment {
    Context context;
    RecyclerView recyclerView;
    ArrayList<Coming_Soon_recy_Modal> movies = new ArrayList<>();
    Coming_soon_recy_Adapter mAdapter;

    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        context = getActivity();
        ViewGroup rootView = (ViewGroup) inflater.inflate(R.layout.hp_comming_soon_frag, container, false);
        recyclerView = rootView.findViewById(R.id.recycler_view);
        mAdapter = new Coming_soon_recy_Adapter(movies,context);
        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(getActivity());
        recyclerView.setLayoutManager(mLayoutManager);
        recyclerView.setItemAnimator(new DefaultItemAnimator());

        recyclerView.setAdapter(mAdapter);
        prepareMovieData();
        return rootView;
    }

    private void prepareMovieData() {
        Coming_Soon_recy_Modal csm = new Coming_Soon_recy_Modal("Sakar","Vijay | Keerthi suresh","Tamil","2D","U/A",R.drawable.img_sarkar,true);
        movies.add(csm);
        csm = new  Coming_Soon_recy_Modal("Kaatrin Mozhi","Jothika | Vidharth","Tamil","2D","U/A",R.drawable.img_katrin_mozhi,true);
        movies.add(csm);
        csm = new  Coming_Soon_recy_Modal("Raatchasan","Vishnu Vishal | Amala Paul","Tamil","2D","U/A",R.drawable.img_raatchasan,true);
        movies.add(csm);
        csm = new  Coming_Soon_recy_Modal("Taxiwaala","Malavika Nair | Priyanka","Telugu","2D","U/A",R.drawable.img_taxiwaala,false);
        movies.add(csm);
        csm = new  Coming_Soon_recy_Modal("Thugs of Hindostan","Amitabh Bachchan | Amir Khan","Hindi / Tamil","2D","U/A",R.drawable.img_toh,true);
        movies.add(csm);
        csm = new  Coming_Soon_recy_Modal("Vada Chennai","Dhanush | Aishwarya Rajesh","Tamil","2D","U/A",R.drawable.img_vada_chn,false);
        movies.add(csm);
        csm = new  Coming_Soon_recy_Modal("Badhaai Ho","Ayushmann Khurrana | Sanya Malhotra","Hindi","2D","U/A",R.drawable.img_badhai_ho,false);
        movies.add(csm);




        mAdapter.notifyDataSetChanged();
    }
}
