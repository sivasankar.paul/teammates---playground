package mates.team.com.myapplication;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RadioGroup;

public class Register_page extends AppCompatActivity {
    EditText et1, et2, et3;
    RadioGroup rg_gen;
    Button button;
    Context context;


    public static final String MYPREFERENCE = "MyPrefs";
    public static final String Reg_UserName = "reg_namekey";
    public static final String Reg_Password = "reg_pswdkey";

    SharedPreferences sharedPreferences;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_register_page);
        context = Register_page.this;

        sharedPreferences = getSharedPreferences(MYPREFERENCE, MODE_PRIVATE);
        Toolbar toolbar =  findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        setTitle(getResources().getString(R.string.register));
        toolbar.setNavigationIcon(R.drawable.arrow_left);
        toolbar.setTitleTextColor(getResources().getColor(R.color.white));

        et1 = findViewById(R.id.User_name);
        et2 = findViewById(R.id.password);
        et3 = findViewById(R.id.c_password);

        rg_gen = findViewById(R.id.rg_Gender);

        button = findViewById(R.id.btn_register);


        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                /*String username_str = et1.getText().toString();
                String password_str = et2.getText().toString();
                String c_password_str = et3.getText().toString();

                int selectedId = rg_gen.getCheckedRadioButtonId();
                RadioButton radioButton = findViewById(selectedId);


                if (username_str.length() == 0) {
                    et1.setError("Username is required");
                } else if (password_str.length() == 0) {
                    et2.setError("Password is required");
                } else if (c_password_str.length() == 0) {
                    et3.setError("confirm password is required");
                }else if(!password_str.equals(c_password_str)){
                    et3.setError("password and confirm password is not matched");
                } else if (selectedId == -1) {
                    Toast.makeText(Register_page.this, "please select gender", Toast.LENGTH_SHORT).show();
                } else if (App_Util.checkConnection(context) ){

                    SharedPreferences.Editor editor = sharedPreferences.edit();
                    editor.putString(Reg_UserName, username_str);
                    editor.putString(Reg_Password, password_str);
                    editor.commit();

                        Log.e("Register", "username " + username_str);
                        Log.e("Register", "password " + password_str);
                        Log.e("Register", "c_password " + c_password_str);
                        Log.e("Register", "Gender " + radioButton.getText());

                        Intent intent = new Intent(Register_page.this, Login.class);
                        startActivity(intent);
                    }
                    else{
                        Toast.makeText(Register_page.this,"Sorry! Not Connected to internet",Toast.LENGTH_SHORT).show();
                    }
*/

                Intent intent = new Intent(Register_page.this, Login.class);
                startActivity(intent);

            }
        });

    }

    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:

                finish();
                return true;

            default:
                return super.onOptionsItemSelected(item);
        }
    }
}
