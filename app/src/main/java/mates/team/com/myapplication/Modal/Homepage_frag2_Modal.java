package mates.team.com.myapplication.Modal;

public class Homepage_frag2_Modal {
    private String heading, option;

    private Homepage_frag2_Modal(){

    }

    public Homepage_frag2_Modal(String hd, String option){
        this.heading = hd;
        this.option = option;
    }

    public String getHeading() {
        return heading;
    }

    public void setHeading(String heading) {
        this.heading = heading;
    }

    public String getOption() {
        return option;
    }

    public void setOption(String option) {
        this.option = option;
    }
}
