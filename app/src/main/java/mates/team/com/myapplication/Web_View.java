package mates.team.com.myapplication;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.webkit.WebView;

public class Web_View extends AppCompatActivity {
    private WebView webview1;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_web__view);

        Toolbar toolbar =  findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        setTitle(getResources().getString(R.string.terms_conditions));
        toolbar.setNavigationIcon(R.drawable.arrow_left);
        toolbar.setTitleTextColor(getResources().getColor(R.color.white));

        webview1 = (WebView) findViewById(R.id.webview);
        webview1.getSettings().setJavaScriptEnabled(true);
        webview1.loadUrl("https://www.google.com");
    }
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:

                finish();
                return true;

            default:
                return super.onOptionsItemSelected(item);
        }
    }
}
