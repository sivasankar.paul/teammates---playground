package mates.team.com.myapplication.Modal;

public class Coming_Soon_recy_Modal {
    private String name, description, language, experience, id;
    private int image1;
    private boolean aBoolean;

    public Coming_Soon_recy_Modal(){}

    public Coming_Soon_recy_Modal(String m_name, String desc, String lang, String exp, String id,int img1, boolean aBoolean){
        this.name = m_name;
        this.description = desc;
        this.language = lang;
        this.experience = exp;
        this.id = id;
        this.image1 = img1;
        this.aBoolean = aBoolean;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getLanguage() {
        return language;
    }

    public void setLanguage(String language) {
        this.language = language;
    }

    public String getExperience() {
        return experience;
    }

    public void setExperience(String experience) {
        this.experience = experience;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public int getImage1() {
        return image1;
    }

    public void setImage1(int image1) {
        this.image1 = image1;
    }

    public boolean getaBoolean() {
        return aBoolean;
    }

    public void setaBoolean(boolean aBoolean) {
        this.aBoolean = aBoolean;
    }



}
