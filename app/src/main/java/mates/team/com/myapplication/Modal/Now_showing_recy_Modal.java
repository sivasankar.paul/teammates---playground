package mates.team.com.myapplication.Modal;

public class Now_showing_recy_Modal {

    private String name, description, language, experience, id;
    private int image;

    public Now_showing_recy_Modal(){}

    public Now_showing_recy_Modal(String m_name, String desc, String lang, String exp, String id,int img){
        this.name = m_name;
        this.description = desc;
        this.language = lang;
        this.experience = exp;
        this.id = id;
        this.image = img;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getLanguage() {
        return language;
    }

    public void setLanguage(String language) {
        this.language = language;
    }

    public String getExperience() {
        return experience;
    }

    public void setExperience(String experience) {
        this.experience = experience;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public int getImage() {
        return image;
    }

    public void setImage(int image) {
        this.image = image;
    }


}
