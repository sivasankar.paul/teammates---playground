package mates.team.com.myapplication.Fragment;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import mates.team.com.myapplication.Adapter.Homepage_frag2_Adapter;
import mates.team.com.myapplication.Modal.Homepage_frag2_Modal;
import mates.team.com.myapplication.R;

import java.util.ArrayList;

public class Homepage_Frag2 extends Fragment {
    TextView tv1,tv2;
    Context context;
    RecyclerView recyclerView;
    ArrayList<Homepage_frag2_Modal> status = new ArrayList<>();
    Homepage_frag2_Adapter mAdapter;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
    ViewGroup rootView = (ViewGroup) inflater.inflate(R.layout.homepage_frag2,container,false);
    context = getActivity();

    tv1 = rootView.findViewById(R.id.clearall);
    tv2 = rootView.findViewById(R.id.tv2);
    tv1.setOnClickListener(new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            status.clear();
            mAdapter.notifyDataSetChanged();
          tv2.setVisibility(View.VISIBLE);
        }
    });

    recyclerView = rootView.findViewById(R.id.recycler_view);
    mAdapter = new Homepage_frag2_Adapter(status,context,tv2);
    RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(getActivity());
    recyclerView.setLayoutManager(mLayoutManager);
    recyclerView.setItemAnimator(new DefaultItemAnimator());

    recyclerView.setAdapter(mAdapter);
    prepareMovieData();
    return rootView;
    }

    private void prepareMovieData() {
        Homepage_frag2_Modal hfm = new Homepage_frag2_Modal("Canceled Today's show due to heavy rain and storm","close");
        status.add(hfm);
        hfm = new Homepage_frag2_Modal("Check upcoming movies!!!","open");
        status.add(hfm);
        hfm = new Homepage_frag2_Modal("Money will be refuned after 3pm","pending");
        status.add(hfm);
        hfm = new Homepage_frag2_Modal("Your ticket has been canceled!!!","open");
        status.add(hfm);
        hfm = new Homepage_frag2_Modal("Win up to 25% cashback on Today's Show!!!","open");
        status.add(hfm);
        hfm = new Homepage_frag2_Modal("Your Password has been changed","pending");
        status.add(hfm);
        hfm = new Homepage_frag2_Modal("check your ticket and seat numbers!!!","close");
        status.add(hfm);
        hfm = new Homepage_frag2_Modal("Your order has been placed!!!","close");
        status.add(hfm);
        hfm = new Homepage_frag2_Modal("welcome to MyTickets!!!","open");
        status.add(hfm);


        mAdapter.notifyDataSetChanged();


    }




}
