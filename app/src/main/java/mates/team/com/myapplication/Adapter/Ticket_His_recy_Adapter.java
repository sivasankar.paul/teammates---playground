package mates.team.com.myapplication.Adapter;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import mates.team.com.myapplication.Modal.Ticket_His_recy_Modal;
import mates.team.com.myapplication.R;

import java.util.ArrayList;

public class Ticket_His_recy_Adapter extends RecyclerView.Adapter<Ticket_His_recy_Adapter.Myholder> {
    ArrayList<Ticket_His_recy_Modal> his_list;
    Context context;

    public Ticket_His_recy_Adapter(ArrayList<Ticket_His_recy_Modal> his_list, Context context) {
        this.his_list = his_list;
        this.context = context;
    }

    @NonNull
    @Override
    public Myholder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View itemView = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.ticket_his_recy_adapter, viewGroup, false);
        return new Ticket_His_recy_Adapter.Myholder(itemView);
    }

    @Override
    public void onBindViewHolder(@NonNull Ticket_His_recy_Adapter.Myholder myholder, int position) {
        myholder.date.setText(his_list.get(position).getDate());
        myholder.month.setText(his_list.get(position).getMonth());
        myholder.m_name.setText(his_list.get(position).getM_name());
        myholder.language.setText(his_list.get(position).getLanguage());
        myholder.status.setText(his_list.get(position).getStatus());
        if (his_list.get(position).getStatus().equalsIgnoreCase("upcoming")) {
            myholder.status.setTextColor(context.getResources().getColor(R.color.green));

        } else if (his_list.get(position).getStatus().equalsIgnoreCase("closed")){
            myholder.status.setTextColor(context.getResources().getColor(R.color.red));

        }else {
            myholder.status.setTextColor(context.getResources().getColor(R.color.red));
        }

    }

    @Override
    public int getItemCount() {
        return his_list.size();
    }

    public class Myholder extends RecyclerView.ViewHolder {
        TextView date, month, m_name, language, status;
        public Myholder(@NonNull View itemView) {
            super(itemView);

            date = (TextView) itemView.findViewById(R.id.date);
            month = (TextView) itemView.findViewById(R.id.month);
            m_name = (TextView) itemView.findViewById(R.id.movie_name);
            language = (TextView) itemView.findViewById(R.id.language);
            status = (TextView) itemView.findViewById(R.id.staus);

        }
    }
}
