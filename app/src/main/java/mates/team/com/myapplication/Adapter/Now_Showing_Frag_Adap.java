package mates.team.com.myapplication.Adapter;

import android.content.Context;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;
import mates.team.com.myapplication.Fragment.Now_Showing_Inner_Frag;
import mates.team.com.myapplication.R;

public class Now_Showing_Frag_Adap extends FragmentStatePagerAdapter {

    Context context;
    public Now_Showing_Frag_Adap(FragmentManager fm, Context context) {
        super(fm);
        this.context = context;
    }


    public Fragment getItem(int position) {
        Now_Showing_Inner_Frag  nsf = new Now_Showing_Inner_Frag();

        int img ;
        if (position == 0) {
            img = R.drawable.bg1;
        } else if (position == 1) {
            img = R.drawable.bg3;
        } else if (position == 2) {
            img = R.drawable.bg1;
        } else {
            img = R.drawable.bg1;
        }

        nsf.methodpassing(position,context,img);
        return nsf;
    }


    @Override
    public int getCount() {
        return 3;
    }
}
