package mates.team.com.myapplication.Adapter;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import mates.team.com.myapplication.Modal.Homepage_frag2_Modal;
import mates.team.com.myapplication.R;

import java.util.ArrayList;

public class Homepage_frag2_Adapter extends RecyclerView.Adapter<Homepage_frag2_Adapter.Myholder> {
    ArrayList<Homepage_frag2_Modal> status;
    Context context;
    TextView textView;

    public Homepage_frag2_Adapter(ArrayList<Homepage_frag2_Modal> status, Context ct, TextView tv2){
        this.status = status;
        this.context = ct;
        this.textView = tv2;

    }
    @NonNull
    @Override
    public Myholder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View itemView = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.homepage_frag2_adapter, viewGroup, false);
        return new Homepage_frag2_Adapter.Myholder(itemView);
    }

    @Override
    public void onBindViewHolder(@NonNull final Myholder myholder, final int position) {
        myholder.heading.setText(status.get(position).getHeading());
        myholder.option.setText(status.get(position).getOption());

        myholder.img.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                status.remove(status.get(position));
                notifyDataSetChanged();
            }
        });
        if(status.get(position).getOption().equalsIgnoreCase("open")){
            myholder.option.setTextColor(context.getResources().getColor(R.color.green));

        }else if(status.get(position).getOption().equalsIgnoreCase("close")) {

            myholder.option.setTextColor(context.getResources().getColor(R.color.red));

        }else if(status.get(position).getOption().equalsIgnoreCase("pending")) {

            myholder.option.setTextColor(context.getResources().getColor(R.color.orange));

        }else {

            myholder.option.setTextColor(context.getResources().getColor(R.color.colorPrimary));

        }

    }

    @Override
    public int getItemCount() {
        if (status.size() == 0){
            textView.setVisibility(View.VISIBLE);
        }
        return status.size();

    }

    public class Myholder extends RecyclerView.ViewHolder {
        TextView heading, option;
        ImageView img;
        public Myholder(@NonNull View itemView) {
            super(itemView);
            heading = (TextView) itemView.findViewById(R.id.tv1);
            option = (TextView) itemView.findViewById(R.id.tv3);
            img = (ImageView) itemView.findViewById(R.id.img_close);
        }
    }
}

