package mates.team.com.myapplication.Fragment;

import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.app.AlertDialog;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.*;
import mates.team.com.myapplication.*;

public class Homepage_Frag3 extends Fragment {
    RelativeLayout rel1,rel2,rel3,rel4,rel5,rel6;
    Context context;
    public static final String MYPREFERENCE = "MyPrefs";
    SharedPreferences sharedpreferences;
    Button button;
    EditText et1,et2;
    ImageView img_close;
    TextView tv, tv1,tv_cp,tv_history,tv_tandc;

    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState){
        context = getActivity();
        ViewGroup rootView = (ViewGroup) inflater.inflate(R.layout.homepage_frag3,container,false);
        tv = rootView.findViewById(R.id.tv_signout);
        tv_cp = rootView.findViewById(R.id.tv_change_pswd);
        tv_history = rootView.findViewById(R.id.tv_tc_his);
        tv_tandc = rootView.findViewById(R.id.tv_tandc);
        rel1 = rootView.findViewById(R.id.rel1);
        rel2 = rootView.findViewById(R.id.rel2);
        rel3 = rootView.findViewById(R.id.rel3);
        rel4 = rootView.findViewById(R.id.rel4);
        rel5 = rootView.findViewById(R.id.rel5);
        rel6 = rootView.findViewById(R.id.rel6);


        rel1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(getActivity(),Change_Password.class);
                startActivity(intent);
            }
        });

//        tv.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {

//            }
//        });
        sharedpreferences = getActivity().getSharedPreferences(MYPREFERENCE,Context.MODE_PRIVATE);
        tv1 = rootView.findViewById(R.id.name);
        String user_name =  sharedpreferences.getString("namekey","");
        Log.e("details","username : "+ user_name);
        tv1.setText(user_name);

        rel6 = rootView.findViewById(R.id.rel6);
        rel6.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                AlertDialog.Builder builder;
                builder = new AlertDialog.Builder(getActivity());
                builder.setCancelable(false)
                        .setMessage("Are you sure you want to exit?")
                        .setPositiveButton(android.R.string.yes, new DialogInterface.OnClickListener() {

                            @Override
                            public void onClick(DialogInterface dialog, int which) {

                                if(App_Util.checkConnection(context) ){
                                    sharedpreferences = getActivity().getSharedPreferences(MYPREFERENCE, Context.MODE_PRIVATE);
                                    SharedPreferences.Editor editor = sharedpreferences.edit();
                                    editor.clear();
                                    editor.commit();
                                    Intent intent = new Intent(getActivity(),Login.class);
                                    startActivity(intent);
                                }
                                else{
                                    Toast.makeText(getActivity(),"Sorry! Not Connected to internet",Toast.LENGTH_SHORT).show();
                                }


                            }
                        })
                        .setNegativeButton(android.R.string.no, new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {

                            }
                        })
                        .show();



            }
        });

        rel2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(getActivity(),Ticket_History.class);
                startActivity(intent);
            }
        });

        rel3.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                final Dialog dialog = new Dialog(context);
                dialog.setContentView(R.layout.custom_popup_feedback);
                dialog.setCancelable(false);
                dialog.setCanceledOnTouchOutside(false);

                img_close  = dialog.findViewById(R.id.img_close);
                img_close.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        dialog.dismiss();
                    }
                });

                et1 = dialog.findViewById(R.id.et1);
                et2 = dialog.findViewById(R.id.et2);
                button = dialog.findViewById(R.id.btn_submit);
                button.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                            String title_str = et1.getText().toString();
                            String feedback_str = et2.getText().toString();

                            if(title_str.length() == 0){
                                et1.setError("Title is required!");
                            }else if(feedback_str.length() == 0){
                                et2.setError("Feedback is required!");
                        } else {
                                Toast.makeText(getActivity(), "Thanks for your feedback", Toast.LENGTH_SHORT).show();
                                dialog.dismiss();
                            }
                    }
                });

                dialog.show();


            }
        });



        rel4.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(getActivity(), Web_View.class);
                startActivity(intent);
            }
        });

        rel5.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(getActivity(), App_Ifo.class);
                startActivity(intent);
            }
        });

        return rootView;
    }
}
