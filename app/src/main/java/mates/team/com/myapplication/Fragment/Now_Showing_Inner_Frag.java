package mates.team.com.myapplication.Fragment;

import android.content.Context;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import mates.team.com.myapplication.R;

public class Now_Showing_Inner_Frag extends Fragment {
    ImageView image;
    int position,img;
    Context context;

    public void methodpassing(int p, Context context,int i){
        this.position = p;
        this.context = context;
        this.img = i;
    }

    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstancesState) {
        ViewGroup rootView = (ViewGroup) inflater.inflate(R.layout.now_showing_inner_frag, container, false);
        context = getActivity();
        image = rootView.findViewById(R.id.img);
        Handler handler = new Handler();

        image.setImageResource(img);

        /*if (position == 0) {
            image.setImageResource(R.drawable.bg1);
        } else if (position == 1) {
            image.setImageResource(R.drawable.bg2);
        } else if (position == 2) {
            image.setImageResource(R.drawable.bg3);
        } else {
            image.setImageResource(R.drawable.bg1);
        }*/

        return rootView;

    }
}
