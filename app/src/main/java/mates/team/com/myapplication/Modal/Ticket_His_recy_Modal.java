package mates.team.com.myapplication.Modal;

public class Ticket_His_recy_Modal {
     private String date, month, m_name, language, status;

    public Ticket_His_recy_Modal(){}

    public Ticket_His_recy_Modal(String date, String month, String name, String language, String status)
    {
        this.date = date;
        this.month = month;
        this.m_name = name;
        this.language = language;
        this.status = status;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public String getMonth() {
        return month;
    }

    public void setMonth(String month) {
        this.month = month;
    }

    public String getM_name() {
        return m_name;
    }

    public void setM_name(String m_name) {
        this.m_name = m_name;
    }

    public String getLanguage() {
        return language;
    }

    public void setLanguage(String language) {
        this.language = language;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }


}
