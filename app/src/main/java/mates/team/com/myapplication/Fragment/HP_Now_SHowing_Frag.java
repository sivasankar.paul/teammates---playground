package mates.team.com.myapplication.Fragment;

import android.content.Context;
import android.graphics.Color;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.view.ViewPager;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Spinner;
import android.widget.TextView;
import mates.team.com.myapplication.Adapter.Now_Showing_Frag_Adap;
import mates.team.com.myapplication.Adapter.Now_showing_recy_Adapter;
import mates.team.com.myapplication.Modal.Now_showing_recy_Modal;
import mates.team.com.myapplication.R;
import com.ravindu1024.indicatorlib.ViewPagerIndicator;

import java.util.ArrayList;
import java.util.Timer;
import java.util.TimerTask;

public class HP_Now_SHowing_Frag extends Fragment {
    private static int NUM_PAGES;
    private ViewPager viewPager;
    Now_Showing_Frag_Adap adapter;
    Context context;
    ArrayList<String> obj,obj2;
    Spinner spin;
    RecyclerView recyclerView;
    ArrayList<Now_showing_recy_Modal> movie = new ArrayList<>();
    Now_showing_recy_Adapter mAdapter;

    int currentPage = 0;
    Timer timer;

    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        context = getActivity();
        ViewGroup rootView = (ViewGroup) inflater.inflate(R.layout.hp_now_showing_frag, container, false);
        Log.e("tab__","tab_11 ");

        viewPager = rootView.findViewById(R.id.tab_pager);
        adapter = new Now_Showing_Frag_Adap(getChildFragmentManager(),context);
        viewPager.setAdapter(adapter);

        ViewPagerIndicator indicator = (ViewPagerIndicator) rootView.findViewById(R.id.pager_indicator);
        indicator.setPager(viewPager);

        final Handler handler = new Handler();
        final Runnable Update = new Runnable() {
            public void run() {
                if (currentPage == 4-1) {
                    currentPage = 0;
                }
                viewPager.setCurrentItem(currentPage++, true);
            }
        };

        timer = new Timer(); // This will create a new Thread
        timer .schedule(new TimerTask() { // task to be scheduled

            @Override
            public void run() {
                handler.post(Update);
            }
        }, 3000, 3000);


        spin = (Spinner) rootView.findViewById(R.id.spinner1);

        obj = new  ArrayList<>();
        obj.add("Language");
        obj.add("All");
        obj.add("English");
        obj.add("Hindi");
        obj.add("Tamil");
        obj.add("Telugu");

        ArrayAdapter aa = new ArrayAdapter(getActivity(),android.R.layout.simple_spinner_item,obj){
            @Override
            public boolean isEnabled(int position) {
                if (position == 0) {
// Disable the first item from Spinner
// First item will be use for hint
                    return false;
                } else {
                    return true;
                }
            }

            @Override
            public View getDropDownView(int position, View convertView, ViewGroup parent) {
                View view = super.getDropDownView(position, convertView, parent);
                TextView tv = (TextView) view;
                if (position == 0) {
// Set the hint text color gray
                    tv.setTextColor(Color.GRAY);
                } else {
                    tv.setTextColor(Color.BLACK);
                }
                return view;
            }
        };


        aa.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spin.setAdapter(aa);

        spin.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {


            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        spin = (Spinner) rootView.findViewById(R.id.spinner2);

        obj2 = new  ArrayList<>();
        obj2.add("Experience");
        obj2.add("All");
        obj2.add("2D");
        obj2.add("3D");

        ArrayAdapter ab = new ArrayAdapter(getActivity(),android.R.layout.simple_spinner_item,obj2){
            @Override
            public boolean isEnabled(int position) {
                if (position == 0) {
// Disable the first item from Spinner
// First item will be use for hint
                    return false;
                } else {
                    return true;
                }
            }

            @Override
            public View getDropDownView(int position, View convertView, ViewGroup parent) {
                View view = super.getDropDownView(position, convertView, parent);
                TextView tv = (TextView) view;
                if (position == 0) {
// Set the hint text color gray
                    tv.setTextColor(Color.GRAY);
                } else {
                    tv.setTextColor(Color.BLACK);
                }
                return view;
            }
        };
        ab.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spin.setAdapter(ab);

        spin.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {

            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        recyclerView = rootView.findViewById(R.id.recycler_view);
        mAdapter = new Now_showing_recy_Adapter(movie,context);
        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(getActivity());
        recyclerView.setLayoutManager(mLayoutManager);
        recyclerView.setItemAnimator(new DefaultItemAnimator());

        recyclerView.setAdapter(mAdapter);
        prepareMovieData();
        return rootView;
    }

    private void prepareMovieData() {
        Now_showing_recy_Modal nsm = new Now_showing_recy_Modal("Sakar","Vijay | Keerthi suresh","Tamil","2D","U/A",R.drawable.img_sarkar);
        movie.add(nsm);
        nsm = new Now_showing_recy_Modal("Kaatrin Mozhi","Jothika | Vidharth","Tamil","2D","U/A",R.drawable.img_katrin_mozhi);
        movie.add(nsm);
        nsm = new Now_showing_recy_Modal("Raatchasan","Vishnu Vishal | Amala Paul","Tamil","2D","U/A",R.drawable.img_raatchasan);
        movie.add(nsm);
        nsm = new Now_showing_recy_Modal("Taxiwaala","Malavika Nair | Priyanka","Telugu","2D","U/A",R.drawable.img_taxiwaala);
        movie.add(nsm);
        nsm = new Now_showing_recy_Modal("Thugs of Hindostan","Amitabh Bachchan | Amir Khan","Hindi / Tamil","2D","U/A",R.drawable.img_toh);
        movie.add(nsm);
        nsm = new Now_showing_recy_Modal("Vada Chennai","Dhanush | Aishwarya Rajesh","Tamil","2D","U/A",R.drawable.img_vada_chn);
        movie.add(nsm);
        nsm = new Now_showing_recy_Modal("Badhaai Ho","Ayushmann Khurrana | Sanya Malhotra","Hindi","2D","U/A",R.drawable.img_badhai_ho);
        movie.add(nsm);



        nsm = new Now_showing_recy_Modal("Kaatrin Mozhi","Jothika | Vidharth","Tamil","2D","U/A",R.drawable.img_katrin_mozhi);
        movie.add(nsm);
        nsm = new Now_showing_recy_Modal("Raatchasan","Vishnu Vishal | Amala Paul","Tamil","2D","U/A",R.drawable.img_raatchasan);
        movie.add(nsm);
        nsm = new Now_showing_recy_Modal("Taxiwaala","Malavika Nair | Priyanka","Telugu","2D","U/A",R.drawable.img_taxiwaala);
        movie.add(nsm);
        nsm = new Now_showing_recy_Modal("Thugs of Hindostan","Amitabh Bachchan | Amir Khan","Hindi / Tamil","2D","U/A",R.drawable.img_toh);
        movie.add(nsm);
        nsm = new Now_showing_recy_Modal("Vada Chennai","Dhanush | Aishwarya Rajesh","Tamil","2D","U/A",R.drawable.img_vada_chn);
        movie.add(nsm);
        nsm = new Now_showing_recy_Modal("Badhaai Ho","Ayushmann Khurrana | Sanya Malhotra","Hindi","2D","U/A",R.drawable.img_badhai_ho);
        movie.add(nsm);
        mAdapter.notifyDataSetChanged();
    }
}
