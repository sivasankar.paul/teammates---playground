package mates.team.com.myapplication.Fragment;

import android.content.Context;
import android.graphics.Color;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Spinner;
import com.google.android.gms.maps.model.CircleOptions;
import mates.team.com.myapplication.R;
import com.google.android.gms.maps.*;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;

import java.util.ArrayList;

public class PlayMap_Frag1  extends Fragment implements OnMapReadyCallback {

    Context context;
    ArrayList<String> obj;
    Spinner spin;
    GoogleMap mMap;

    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        context = getActivity();
        ViewGroup rootView = (ViewGroup) inflater.inflate(R.layout.playmap_frag1, container, false);

        spin = (Spinner) rootView.findViewById(R.id.spinner);

        obj = new ArrayList<>();
        obj.add("Theni");
        obj.add("Cumbum");
        obj.add("Madurai");
        obj.add("Chennai");
        obj.add("Coimbatore");

        ArrayAdapter aa = new ArrayAdapter(getActivity(),R.layout.spinner_item,obj);
        aa.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        //Setting the ArrayAdapter data on the Spinner
        spin.setAdapter(aa);

        spin.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        // Google Maps
        SupportMapFragment mapFragment = (SupportMapFragment) getChildFragmentManager().findFragmentById(R.id.map);
        mapFragment.getMapAsync(this);


        return rootView;
    }

    @Override
    public void onMapReady(GoogleMap googleMap) {
        mMap = googleMap;

        // Lat and Long
        LatLng latLng = new LatLng(12.974759, 80.154308);

        // Add a Marker
        mMap.addMarker(new MarkerOptions().position(latLng).title("Siva - Badminton - Intermediate"));

        // Move camera
        CameraUpdate center = CameraUpdateFactory.newLatLng(latLng);
        mMap.moveCamera(center);

        // Zooming
        CameraUpdate zoom = CameraUpdateFactory.zoomTo(15);
        mMap.animateCamera(zoom);

        // Circle
        mMap.addCircle(new CircleOptions()
                .center(latLng)
                .radius(500)
                .strokeWidth(0f)
                .strokeColor(Color.RED)
                .fillColor(0x550000FF));


    }



}
