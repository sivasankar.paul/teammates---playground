package mates.team.com.myapplication.Fragment;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.view.PagerAdapter;
import android.support.v4.view.ViewPager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Spinner;
import mates.team.com.myapplication.Adapter.Homepage_frag1_Adapter;
import mates.team.com.myapplication.R;

import java.util.ArrayList;

public class Homepage_Frag1 extends Fragment {
    ViewPager viewPager;
    TabLayout tabLayout;
    PagerAdapter adapter;
    Context context;
    ArrayList<String> obj;
    Spinner spin;


    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        context = getActivity();
        ViewGroup rootView = (ViewGroup) inflater.inflate(R.layout.homepage_frag1, container, false);

        spin = (Spinner) rootView.findViewById(R.id.spinner);

        obj = new ArrayList<>();
        obj.add("Chennai");
        obj.add("Bangalore");
        obj.add("Coimbatore");
        obj.add("Hyderabad");
        obj.add("Madurai");

        ArrayAdapter aa = new ArrayAdapter(getActivity(), R.layout.spinner_item, obj);
        aa.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        //Setting the ArrayAdapter data on the Spinner
        spin.setAdapter(aa);

        spin.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {

            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });


        viewPager = rootView.findViewById(R.id.tab_viewpager);
        tabLayout = rootView.findViewById(R.id.tablayout);

        tabLayout.addTab(tabLayout.newTab().setText("NOW SHOWING"));
        tabLayout.addTab(tabLayout.newTab().setText("COMING SOON"));
        tabLayout.setTabGravity(TabLayout.GRAVITY_FILL);

        adapter = new Homepage_frag1_Adapter(getChildFragmentManager(), tabLayout.getTabCount());
        viewPager.setAdapter(adapter);
        viewPager.addOnPageChangeListener(new TabLayout.TabLayoutOnPageChangeListener(tabLayout));
        tabLayout.addOnTabSelectedListener(new TabLayout.BaseOnTabSelectedListener() {
            @Override
            public void onTabSelected(TabLayout.Tab tab) {
                viewPager.setCurrentItem(tab.getPosition());
            }

            @Override
            public void onTabUnselected(TabLayout.Tab tab) {
            }

            @Override
            public void onTabReselected(TabLayout.Tab tab) {
            }
        });
        return rootView;
    }
}
