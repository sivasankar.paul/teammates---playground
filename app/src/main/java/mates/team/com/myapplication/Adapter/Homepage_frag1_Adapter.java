package mates.team.com.myapplication.Adapter;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;
import android.util.Log;
import mates.team.com.myapplication.Fragment.HP_Coming_Soon_Frag;
import mates.team.com.myapplication.Fragment.HP_Now_SHowing_Frag;

public class Homepage_frag1_Adapter extends FragmentStatePagerAdapter {
    int mNumofTabs;

    public Homepage_frag1_Adapter(FragmentManager fm, int NumofTabs) {
        super(fm);
        this.mNumofTabs = NumofTabs;
    }


    public Fragment getItem(int position) {
        switch (position) {
            case 0:
                Log.e("tab__","tab1 "+position);
                HP_Now_SHowing_Frag tab1 = new HP_Now_SHowing_Frag();
                return tab1;
            case 1:
                Log.e("tab__","tab1 "+position);

                HP_Coming_Soon_Frag tab2 = new HP_Coming_Soon_Frag();
                return tab2;
            default:
                return null;
        }
    }

    public int getCount() {
        return mNumofTabs;
    }
}
