package mates.team.com.myapplication.Adapter;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;
import mates.team.com.myapplication.Fragment.Homepage_Frag2;
import mates.team.com.myapplication.Fragment.Homepage_Frag3;
import mates.team.com.myapplication.Fragment.PlayMap_Frag1;

public class Home_page_Adapter extends FragmentStatePagerAdapter {
int mNumofTabs;
    public Home_page_Adapter(FragmentManager fm, int NumofTabs) {
        super(fm);
        this.mNumofTabs = NumofTabs;
    }

    @Override
    public Fragment getItem(int position) {
        switch (position){
            case 0:
                PlayMap_Frag1 tab1 = new PlayMap_Frag1();
                return tab1;
            case 1:
                Homepage_Frag2 tab2 = new Homepage_Frag2();
                return tab2;
            case 2:
                Homepage_Frag3 tab3 = new Homepage_Frag3();
                return tab3;
            default:
            return null;

        }
    }

    @Override
    public int getCount() {
        return mNumofTabs;
    }
}
