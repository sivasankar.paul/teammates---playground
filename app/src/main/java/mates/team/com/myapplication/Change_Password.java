package mates.team.com.myapplication;

import android.app.ProgressDialog;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

public class Change_Password extends AppCompatActivity {
    EditText et1, et2, et3;
    Button button;
    SharedPreferences sharedPreferences;
    public static final String MYPREFERENCE = "MyPrefs";
    public static final String Password = "pswdkey";
    String old_password;
    ProgressDialog progressDialog;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_change__password);
        Toolbar toolbar =  findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        setTitle(getResources().getString(R.string.change_password));
        toolbar.setNavigationIcon(R.drawable.arrow_left);
        toolbar.setTitleTextColor(getResources().getColor(R.color.white));
//        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                Intent intent = new Intent(Change_Password.this,Homepage_Frag3.class);
//                startActivity(intent);
//            }
//        });

        sharedPreferences = getSharedPreferences(MYPREFERENCE, MODE_PRIVATE);
        old_password = sharedPreferences.getString("pswdkey", "");
        Log.e("Change_Password", "old_password: " + old_password);


        et1 = findViewById(R.id.old_pswd);
        et2 = findViewById(R.id.new_password);
        et3 = findViewById(R.id.c_password);
        button = findViewById(R.id.btn_update);

        et1.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View v, boolean hasFocus) {
                if (!hasFocus) {
                    String o_pswd_str = et1.getText().toString();
                    if (!old_password.equals(o_pswd_str)) {
                        et1.setError("doesn't matching");
                    }
                }
            }
        });

        /*et1.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                Log.e("Change_Password", "et1: " + s.toString());
                if (!old_password.equals(s.toString())) {
                    et1.setError("doesn't matching");
                }
            }
        });*/

        button.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                //finish();
                String o_pswd_str = et1.getText().toString();
                String n_pwsd_str = et2.getText().toString();
                String c_pswd_str = et3.getText().toString();


                if (o_pswd_str.length() == 0) {
                    et1.setError("Old Password is required!");
                } else if (n_pwsd_str.length() == 0) {
                    et2.setError("Enter your new passord!");
                } else if (c_pswd_str.length() == 0) {
                    et3.setError("Enter your new password again!");
                } else if (!old_password.equals(o_pswd_str)) {
                    et1.setError("doesn't matching");
                } else if (!c_pswd_str.equals(n_pwsd_str)) {
                    et3.setError("password and confirm password doest match!");
                } else {
                    progressDialog = new ProgressDialog(Change_Password.this);
                    progressDialog.setMessage("Loading...");
                    progressDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
                    progressDialog.show();
                    progressDialog.setCancelable(false);

                    Toast.makeText(Change_Password.this,"password changed susccessfully!",Toast.LENGTH_SHORT).show();
                    Log.e("change_password", "success: " + n_pwsd_str);
                    SharedPreferences.Editor editor = sharedPreferences.edit();
                    editor.putString(Password, c_pswd_str);
                    editor.commit();
                    finish();
                }
            }


        });

    }
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:

                finish();
                return true;

            default:
                return super.onOptionsItemSelected(item);
        }
    }
}
