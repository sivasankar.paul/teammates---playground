package mates.team.com.myapplication;

import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.*;

import java.util.regex.Pattern;

public class Login extends AppCompatActivity {
    Context context = this;

    private EditText et1,et2,et3;
    private Button button,button1;
    ImageView img_close;
    TextView tv1,tv2,tv3;

    public static final String MYPREFERENCE = "MyPrefs";
    public static final String UserName = "namekey";
    public static final String Password = "pswdkey";


    SharedPreferences sharedPreferences;
    String reg_username,reg_password;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        et1 = findViewById(R.id.User_name);
        et2 = findViewById(R.id.password);

        button = findViewById(R.id.btn_login);

        tv1 = findViewById(R.id.tv_forgot_pswd);
        tv2 = findViewById(R.id.tv_no_acc);
        tv3 = findViewById(R.id.tv_signup);

        sharedPreferences = getSharedPreferences(MYPREFERENCE, MODE_PRIVATE);
        reg_username = sharedPreferences.getString("reg_namekey", "");
        reg_password = sharedPreferences.getString("reg_pswdkey", "");

       /* et1.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View v, boolean hasFocus) {
                if (!hasFocus) {
                    String final_username = et1.getText().toString();
                    if (!reg_username.equals(final_username)) {
                        et1.setError("doesn't matching");
                    }
                }
            }
        });

        et2.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View v, boolean hasFocus) {
                if (!hasFocus) {
                    String final_password = et2.getText().toString();
                    if (!reg_password.equals(final_password)) {
                        et2.setError("doesn't matching");
                    }
                }
            }
        });*/


        tv3.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(Login.this,Register_page.class);
                startActivity(intent);
            }
        });


        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                String final_username = et1.getText().toString();
                String final_password = et2.getText().toString();

                if (final_username.length() == 0) {
                    et1.setError("Username is required");
                } else if (final_password.length() == 0) {
                    et2.setError("Password is required");


                /*}else  if (!reg_username.equals(final_username)) {
                    et1.setError("doesn't matching your registered username");
                }else if (!reg_password.equals(final_password)) {
                    et2.setError("doesn't matching your registered password");*/

                } else if(App_Util.checkConnection(context) ){

                        Toast.makeText(Login.this, "success", Toast.LENGTH_SHORT).show();
                        SharedPreferences.Editor editor = sharedPreferences.edit();
                        editor.putString(UserName, final_username);
                        editor.putString(Password, final_password);
                        editor.putBoolean("is_login", true);
                        editor.commit();

                        Intent intent = new Intent(Login.this, Home_page.class);
                        startActivity(intent);
                    }
                    else{
                        Toast.makeText(Login.this,"Sorry! Not Connected to internet",Toast.LENGTH_SHORT).show();
                    }

            }
        });

        tv1.setOnClickListener(new View.OnClickListener() {


            @Override
            public void onClick(View v) {
                 final Pattern EMAIL_ADDRESS_PATTERN = Pattern.compile(
                        "[a-zA-Z0-9+._%-+]{1,256}" +
                                "@" +
                                "[a-zA-Z0-9][a-zA-Z0-9-]{0,64}" +
                                "(" +
                                "." +
                                "[a-zA-Z0-9][a-zA-Z0-9-]{0,25}" +
                                ")+"
                );
                final Dialog dialog = new Dialog(context);
                dialog.setContentView(R.layout.forgot_pswd_popup);
                dialog.setCancelable(false);
                dialog.setCanceledOnTouchOutside(true);

                tv1 = dialog.findViewById(R.id.tv_acc_rec);
                et3 = dialog.findViewById(R.id.et_email);
                img_close = dialog.findViewById(R.id.btn_close);


                button1 = dialog.findViewById(R.id.btn_submit);

                button1.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        String email=et3.getText().toString();
                        if(email.length() == 0){
                            et3.setError("Enter your email address!");
                        }else if(checkEmail(email)) {
                            Toast.makeText(Login.this, "Valid Email Addresss", Toast.LENGTH_SHORT).show();
                            dialog.dismiss();
                        } else {
                            Toast.makeText(Login.this, "Invalid Email Addresss", Toast.LENGTH_SHORT).show();
                        }

                    }


                    private boolean checkEmail(String email) {
                        return EMAIL_ADDRESS_PATTERN.matcher(email).matches();
                    }

                });

                img_close.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        dialog.dismiss();
                    }
                });

                dialog.show();


            }


        });
        
    }

    

//    private  boolean is_empty(String str){
//        return str.length() == 0 || str == "null"  || str == null;
//    }
//    private boolean checkDataEntered() {
//        boolean is_valid = false;
//
//        if ((!is_empty(final_username)) && (!is_empty(final_password))){
//            is_valid = true;
//            Log.e("login","if_ "+ is_valid);
//        } else {
//            is_valid = false;
//            Log.e("login","else_ "+ is_valid);
//        }
//            return is_valid;
//    }

    public void onBackPressed() {
        Intent intent = new Intent(Intent.ACTION_MAIN);
        intent.addCategory(Intent.CATEGORY_HOME);
        intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);//***Change Here***
        startActivity(intent);
    }
}
