package mates.team.com.myapplication;

import android.content.Context;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import mates.team.com.myapplication.Adapter.Ticket_His_recy_Adapter;
import mates.team.com.myapplication.Modal.Ticket_His_recy_Modal;

import java.util.ArrayList;

public class Ticket_History extends AppCompatActivity {

    Context context;
    RecyclerView recyclerView;
    ArrayList<Ticket_His_recy_Modal> his_list = new ArrayList<>();
    Ticket_His_recy_Adapter mAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_ticket__history);

        Toolbar toolbar =  findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        setTitle(getResources().getString(R.string.ticket_history));
        toolbar.setNavigationIcon(R.drawable.arrow_left);
        toolbar.setTitleTextColor(getResources().getColor(R.color.white));

        context = Ticket_History.this;

        recyclerView = findViewById(R.id.recycler_view);
        mAdapter = new Ticket_His_recy_Adapter(his_list,context);
        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(Ticket_History.this);
        recyclerView.setLayoutManager(layoutManager);
        recyclerView.setItemAnimator(new DefaultItemAnimator());
        recyclerView.setAdapter(mAdapter);

        PrepareMovieData();
    }

    private void PrepareMovieData() {
        Ticket_His_recy_Modal thm = new Ticket_His_recy_Modal("15","OCT","Sarkar","Tamil","closed");
        his_list.add(thm);
        thm = new Ticket_His_recy_Modal("14","OCT","Kaatrin Mozhi","Tamil","upcoming");
        his_list.add(thm);
        thm = new Ticket_His_recy_Modal("12","OCT","Raatchasan","Tamil","closed");
        his_list.add(thm);
        thm = new Ticket_His_recy_Modal("01","OCT","96","Tamil","closed");
        his_list.add(thm);
        thm = new Ticket_His_recy_Modal("28","SEP","Sandakozhi 2","Tamil","upcoming");
        his_list.add(thm);
        thm = new Ticket_His_recy_Modal("15","SEP","Chekka Chivantha Vanam","Tamil","upcoming");
        his_list.add(thm);
        thm = new Ticket_His_recy_Modal("07","SEP","Samy 2","Tamil","closed");
        his_list.add(thm);
    }

    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                // todo: goto back activity from here

//                Intent intent = new Intent(Change_Password.this, Homepage_Frag3.class);
//                intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK);
//                startActivity(intent);
                finish();
                return true;

            default:
                return super.onOptionsItemSelected(item);
        }
    }
}
